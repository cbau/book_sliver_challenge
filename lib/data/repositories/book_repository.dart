import 'dart:convert';

import 'package:book_sliver_challenge/data/models/book.dart';
import 'package:flutter/services.dart';

class BookRepository {
  static final _instance = BookRepository._();

  List<Book>? _items;

  BookRepository._();

  factory BookRepository() => _instance;

  Future<List<Book>> get all async {
    if (_items == null) {
      final jsonString =
          await rootBundle.loadString('assets/flutter-books.json');
      final map = json.decode(jsonString) as List<Object?>;
      _items =
          map.map((e) => Book.fromMap(e as Map<String, Object?>?)).toList();
    }

    return _items ?? [];
  }

  Book? get(String id) {
    try {
      return _items?.firstWhere((e) => e.id == id);
    } catch (e) {
      return null;
    }
  }
}
