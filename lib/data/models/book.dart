class Book {
  final List<String> authors;
  final String canonicalVolumeLink;
  final String description;
  final String id;
  final String infoLink;
  final String language;
  final String previewLink;
  final String publisher;
  final String publishedDate;
  final String smallThumbnail;
  final String subtitle;
  final String thumbnail;
  final String title;

  const Book({
    required this.authors,
    required this.canonicalVolumeLink,
    required this.description,
    required this.id,
    required this.infoLink,
    required this.language,
    required this.previewLink,
    required this.publisher,
    required this.publishedDate,
    required this.smallThumbnail,
    required this.subtitle,
    required this.thumbnail,
    required this.title,
  });

  const Book.empty()
      : this(
          authors: const [],
          canonicalVolumeLink: '',
          description: '',
          id: '',
          infoLink: '',
          language: '',
          previewLink: '',
          publisher: '',
          publishedDate: '',
          smallThumbnail: '',
          subtitle: '',
          thumbnail: '',
          title: '',
        );

  Book.fromMap(Map<String, Object?>? map)
      : this(
          authors: (map?['authors'] as List?)?.cast() ?? [],
          canonicalVolumeLink: map?['canonicalVolumeLink'] as String? ?? '',
          description: map?['description'] as String? ?? '',
          id: map?['id'] as String? ?? '',
          infoLink: map?['infoLink'] as String? ?? '',
          language: map?['language'] as String? ?? '',
          previewLink: map?['previewLink'] as String? ?? '',
          publisher: map?['publisher'] as String? ?? '',
          publishedDate: map?['publishedDate'] as String? ?? '',
          smallThumbnail: map?['smallThumbnail'] as String? ?? '',
          subtitle: map?['subtitle'] as String? ?? '',
          thumbnail: map?['thumbnail'] as String? ?? '',
          title: map?['title'] as String? ?? '',
        );

  String get author {
    return authors.join(', ');
  }
}
