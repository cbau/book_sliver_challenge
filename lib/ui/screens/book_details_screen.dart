import 'package:book_sliver_challenge/data/models/book.dart';
import 'package:book_sliver_challenge/data/repositories/book_repository.dart';
import 'package:book_sliver_challenge/ui/screens/not_found_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class BookDetailsScreen extends StatefulWidget {
  static const routeName = '/books/:id';

  final String itemId;
  final Book? item;

  const BookDetailsScreen({
    required this.itemId,
    this.item,
    Key? key,
  }) : super(key: key);

  @override
  State<BookDetailsScreen> createState() => _BookDetailsScreenState();
}

class _BookDetailsScreenState extends State<BookDetailsScreen> {
  final _scrollController = ScrollController(
    initialScrollOffset: 300,
  );
  Book? _item;

  @override
  Widget build(BuildContext context) {
    return _item == null
        ? const NotFoundScreen()
        : Scaffold(
            body: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  SliverOverlapAbsorber(
                    handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                        context),
                    sliver: SliverAppBar(
                      expandedHeight:
                          _item!.smallThumbnail.isEmpty ? null : 480,
                      flexibleSpace: FlexibleSpaceBar(
                        background: _item!.smallThumbnail.isEmpty
                            ? null
                            : Hero(
                                tag: 'thumbnail${_item!.id}',
                                child: CachedNetworkImage(
                                  color: Theme.of(context).primaryColor,
                                  colorBlendMode: BlendMode.multiply,
                                  fit: BoxFit.cover,
                                  imageUrl: _item!.smallThumbnail,
                                  width: MediaQuery.of(context).size.width,
                                ),
                              ),
                        title: Text(
                          _item!.title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      forceElevated: innerBoxIsScrolled,
                      pinned: true,
                    ),
                  ),
                ];
              },
              body: Builder(
                  builder: (context) => CustomScrollView(
                        slivers: [
                          SliverOverlapInjector(
                              handle: NestedScrollView
                                  .sliverOverlapAbsorberHandleFor(context)),
                          SliverPadding(
                            padding: const EdgeInsets.all(16),
                            sliver: SliverList(
                              delegate: SliverChildListDelegate(
                                [
                                  Text(
                                    _item!.title,
                                    style:
                                        Theme.of(context).textTheme.headline6,
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    _item!.author,
                                    style: Theme.of(context).textTheme.caption,
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    _item!.description,
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
            ),
          );
  }

  @override
  void initState() {
    super.initState();

    _item = widget.item ?? BookRepository().get(widget.itemId);
  }
}
