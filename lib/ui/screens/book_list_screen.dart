import 'package:book_sliver_challenge/data/models/book.dart';
import 'package:book_sliver_challenge/ui/presenters/book_list_presenter.dart';
import 'package:book_sliver_challenge/ui/widgets/book_item.dart';
import 'package:flutter/material.dart';

class BookListScreen extends StatefulWidget {
  static const routeName = '/';

  const BookListScreen({Key? key}) : super(key: key);

  @override
  State<BookListScreen> createState() => _BookListScreenState();
}

class _BookListScreenState extends State<BookListScreen> {
  final presenter = BookListPresenter();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<List<Book>>(
          stream: presenter.bookStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return CustomScrollView(
                slivers: [
                  const SliverAppBar(
                    floating: true,
                    snap: true,
                    title: Text('Books'),
                  ),
                  SliverPadding(
                    padding: const EdgeInsets.all(8),
                    sliver: SliverGrid(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) =>
                            BookItem(item: snapshot.data![index]),
                        childCount: snapshot.data!.length,
                      ),
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        childAspectRatio: 0.6,
                        crossAxisSpacing: 8,
                        mainAxisSpacing: 8,
                        maxCrossAxisExtent: 120,
                      ),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error!.toString()),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  @override
  void dispose() {
    super.dispose();

    presenter.dispose();
  }

  @override
  void initState() {
    super.initState();

    presenter.fetch();
  }
}
