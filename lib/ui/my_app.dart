import 'package:book_sliver_challenge/ui/app_router.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: AppRouter().routerDelegate,
      routeInformationParser: AppRouter().routeInformationParser,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      title: 'Book Sliver Challenge',
    );
  }
}
