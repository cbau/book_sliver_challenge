import 'package:book_sliver_challenge/ui/screens/book_details_screen.dart';
import 'package:book_sliver_challenge/ui/screens/book_list_screen.dart';
import 'package:book_sliver_challenge/ui/screens/not_found_screen.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class AppRouter {
  static final AppRouter _instance = AppRouter._();

  AppRouter._();

  factory AppRouter() => _instance;

  final _router = GoRouter(
    errorBuilder: (context, state) => const NotFoundScreen(),
    routes: [
      GoRoute(
        builder: (context, state) => const BookListScreen(),
        path: BookListScreen.routeName,
      ),
      GoRoute(
        builder: (context, state) => BookDetailsScreen(
          itemId: state.params['id'] ?? '',
        ),
        path: BookDetailsScreen.routeName,
      ),
    ],
  );

  RouterDelegate<Object> get routerDelegate => _router.routerDelegate;

  RouteInformationParser<Object> get routeInformationParser =>
      _router.routeInformationParser;
}
