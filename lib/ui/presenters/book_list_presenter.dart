import 'dart:async';

import 'package:book_sliver_challenge/data/models/book.dart';
import 'package:book_sliver_challenge/data/repositories/book_repository.dart';

class BookListPresenter {
  final _repository = BookRepository();
  final _streamController = StreamController<List<Book>>.broadcast();

  Stream<List<Book>> get bookStream => _streamController.stream;

  void dispose() {
    _streamController.close();
  }

  Future<void> fetch() async {
    try {
      final items = await _repository.all;
      _streamController.sink.add(items);
    } catch (e) {
      _streamController.sink.addError(e);
    }
  }
}
