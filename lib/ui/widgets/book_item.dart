import 'package:book_sliver_challenge/data/models/book.dart';
import 'package:book_sliver_challenge/ui/screens/book_details_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BookItem extends StatelessWidget {
  final Book item;

  const BookItem({
    required this.item,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () => GoRouter.of(context).push(
          BookDetailsScreen.routeName.replaceFirst(':id', item.id),
          extra: item,
        ),
        child: Column(
          children: [
            Expanded(
              child: item.smallThumbnail.isEmpty
                  ? const SizedBox()
                  : Hero(
                      tag: 'thumbnail${item.id}',
                      child: CachedNetworkImage(
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.broken_image),
                        fit: BoxFit.cover,
                        imageUrl: item.smallThumbnail,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Center(
                          child: CircularProgressIndicator(
                            value: downloadProgress.progress,
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Text(
                '${item.title}\n',
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
