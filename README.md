# Book Sliver Challenge

Flutter challenge displaying a book grid using slivers.

## Features

- The app is created as a sliver capabilities showcase.
- The app bar on the main screen hides on scroll down, and reappears con scroll up.
- Each element is tappable, displaying the book in detail in the next screen.
- The image in the details screen collapses into an app bar on scroll down, and reappears on scroll up.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/book_sliver_challenge/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
